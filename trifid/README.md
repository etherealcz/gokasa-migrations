# Migration from Trifid

## Export:
- Go to: Hlavní nabídka / SYSTÉMOVÉ FUNKCE / Zálohování dat
- Select export destination, e.g. Desktop
- Copy the zip file over to your computer (you can use Teamviewer File transfer)

## Conversion
- Extract files:
    - SKLAD/Zbozi.DBF,
    - SKLAD/PCeny.DBF,
    - DOKLADY/PlUcDD.DBF,
    - DOKL01/PlUcDD.DBF
- Go to https://onlineconvertfree.com/convert/dbf/ and convert these two files to *XLSX*
- Or use `dbf2csv.exe` to convert them in the same folder, just run it there

## Transformation
- Open `Zbozi.xlsx`
- Delete all columns except: 
    - `A` - `CISLO,C,5`
    - `B` - `CISLO_VYR,C,15`
    - `F` - `SKUPINA,C,10`
    - `N` - `DL_NAZEV,C,35`
- Insert table with headers
- Search and remove all whitespaces in column `CISLO,C,5`
- Search and replace wierd symbols in column `DL_NAZEV,C,35` with equivalent Czech letter, e.g. `wierd 2` = `ř`, `Ï` = `ě`, etc. Then check if the names are OK.
- Change the order of columns to: `CISLO,C,5`, `CISLO_VYR,C,15`, `DL_NAZEV,C,35`, `SKUPINA,C,10`
- Add column `price` after column `DL_NAZEV,C,35`
- Open `PCeny.xlsx`
- Delete all columns except: 
    - `B` - `CISLO,C,5`
    - `C` - `CENOVA_SAZ,C,2`
    - `D` - `CENA_P_BEZ,C,11`
    - `E` - `DPH,C,1`
- Insert table with headers
- Remove all rows that have value `02`, `03`, `04` in column `CENOVA_SAZ,C,2`
- Convert values in column `CENA_P_BEZ,C,11` to numbers, replace leading zeros and decimal point if necessary
- Replace values in column `DPH,C,1` as follows:
    - `0` = `0`
    - `1` = `21`
    - `2` = `15`
    - `3` = `10`
- Calculate price incl. VAT in column `E` with the following formula
    - *`=[@[CENA_P_BEZ,C,11]]*((100+[@DPH,C,1])/100)`*
- Check the calculated prices - all price should end with a `0`, otherwise you messed up the tax rates
- Copy this table over to file `Zbozi.xlsx` in a new sheet
- Go back to the first sheet and reference the prices in column `price` using the following formula
    - *`=VLOOKUP([@CISLO,C,5];Table2;5;FALSE)`*, replace the table name if necessary
- Now we need to reduce the number of products for import, we can use the transaction data in the `PlUcDD.csv` file, open it in Excel and copy the 4th column which has values like `Z     5`, etc.
- Remove all non-numeric characters from this column
- Then remove all duplicate values in this column
- Take the result and filter the product data with it - take only rows with `CISLO,C,5` values that exist in this list, you can use `VLOOKUP` for this
- Do some cleaning if necessary
    - Remove duplicates from column `CISLO_VYR,C,15`
    - Filter column `price` so zero prices do not show
    - Filter column `CISLO_VYR,C,15` so wierd ean codes do not show
    - Filter column `DL_NAZEV,C,35` so wierd product names do not show
- Copy and paste VALUES from this table to a new sheet
- Insert table with headers and rename the column headers to `ean`, `name`, `price`, `group`, `amount`, `unit`
- Copy all rows with columns `ean`, `name`, `price`, `group` to a text file, name it `input.csv` and save it in the same folder
- run the following command in `cmd` or Powershell (you need to have node.js installed in your system Path)
    - `$> node.exe addAmountAndUnitFromName input.csv 1`
    - This will create a file called `out.csv` with columns `amount` and `unit` filled
- Copy the `out.csv` file content back to Excel in a new sheet
- Insert table with headers and rename the columns headers to `ean`, `name`, `price`, `group`, `amount`, `unit`
- Change the values of column `group` to numbers that match Gokasa product groups
- Copy and pastethe result table to a text file and save it as `import.csv`
- Import this file to Gokasa









