# Migration from VSS Shop

- We want to export a catalog from VSS - Shopping software to import into GOKASA.
- Examine the database at `http://localhost/phpmyadmin`
    - The original catalog is too large. How to find only relevant items which are used?
    - The relevant items are the items which are used in orders over a recent period of time (e.g. last year until today).
    - The number of relevant items should be significantly less than the total number of items in the catalog.
- Decide which order date will be used and find that order in the `orders` database table, in this case we will use `2021-01-01`
- Open SQL tab and run query `SELECT * FROM orders WHERE orderDate > DATE('2021-01-01')` (replace your date here)
- Copy the largest `orderId` in the result table, in this case `366475`
- Run the following SQL query in the SQL tab, replace the number `366475` with whatever you found

---
```sql

SELECT 
  DISTINCT hang.code, 
  hang.name, 
  hang.sellPrice, 
  categories.cateName 
FROM `orderitems`
INNER JOIN hang ON orderitems.itemId=hang.id
INNER JOIN categories ON hang.cateId=categories.cateId
WHERE 
  orderitems.orderId > 366475 AND 
  hang.sellPrice > 0 AND
  hang.quantity <= 0 AND
  LENGTH(hang.code) >= 8 AND
  LENGTH(hang.code) <= 13
```
---

- Export the result as CSV in file `orders.csv`
    - click on **Export** near the bottom of page (operations for the query results)
    - choose/specify options
        - export method **Custom**
        - choose format **CSV**, 
        - file encoding **UTF-8**
        - escape character **EMPTY**
        - wrap character **EMPTY**
        - delimiter **\t**
        - remove new line characters from field **YES**
        - add CSV file header **YES**
- Upload file `orders.csv` to partners/register setting/Add amount and unit to catalog
    - This will give you file `catalog.csv` including the last two columns (amount and unit)
- Import file `catalog.csv` in Excel or Google Sheets (Ctrl+A, Ctrl+C, Ctrl+V)
- Change csv header to `ean`, `name`, `price`, `group`, `amount`, `unit`
- Clean the content
    - replace unreadable characters with Czech characters ěščřžýáíéóúůňťďĚŠČŘŽÝÁÍÉÓÚŮŇŤĎ (use deduction)
    - change price format so it has two decimal numbers
- Change the values of column `group` to numbers that match Gokasa product groups
- Copy and paste the result table to the `catalog.csv` text file and save it
- Import this file to Gokasa

