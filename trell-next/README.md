# TRELL-NEXT

![image](https://github.com/nvbach91/nvbach91/assets/20724910/dd77cc9d-589b-4dff-8d87-dbb44a89cc09)

- open the Stock app
- click export data CSV
- you will get a CSV file like this
```
8595625600800;vino;1 bile 1,5L;24,90;0
8595625600817;vino;1 cervene 1,5L;24,90;0
6224007527023;banh keo;1 TIME 16g;4,90;0
6224007527047;banh keo;1 TIME 16g;4,00;0
6224007527009;banh keo;1 TIME 16g;4,90;0
6224007527054;banh keo;1 TIME 16g;4,90;0
8594056260065;banh mi;1/2 balene chleb 0,46kg;24,90;-24
```
- import it to excel - change the data type of all the columns to text
- rename columns
    - first column to `ean`
    - second column to `group`
    - third column to `name`
    - fourth column to `price`
- delete the last column (stock balance)
- move the `group` column to the fourth position
- remove all duplicate rows by the `ean` column
- remove all rows that contains non-numeric characters in the `ean` column
    - use the formula `=ISNUMBER(1*[@ean])` to do it
- remove all rows that contains less than 6 numbers
    - use the formula `=LEN([@ean])` to do it
- delete `ISNUMBER` and `LEN` columns
- remove all rows that has `price` = 0
- remove all quotes (") and semicolons (;)
- *dont forget to save the file frequently*
- get the list of distinc `group` values and create a new table on a new sheet
    - do this by removing duplicate values in the `group` column
- insert the GOKASA `group` numbers to the second column of this table
- use VLOOKUP to change the old `group` values to the new GOKASA `group` numbers
    - `=VLOOKUP([@group];Table2;2;FALSE)`
- check that the new GOKASA `group` number column doesn't have any `#N/A` values
- copy the new GOKASA `group` number column to notepad
- delete the new GOKASA `group` number column
- copy the notepad content and paste to the `group` column
- copy the excel table to notepad and save it as `input.csv`
- run `node addAmountAndUnitFromName input.csv 1`
- check and fix the out.csv file
    - fix headers
    - append GOKASA default PLUs (codes that have 1-3 digits)
    - this is the file for import to GOKASA
