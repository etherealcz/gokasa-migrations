# Extract amount and unit from product name

Prerequisites:
1. Install node.js LTS on your computer (https://nodejs.org)
2. Verify the installation by opening a CMD window and enter node -v
3. The input CSV file must be semi-colon or tab separated

Usage:
1. Create a folder and copy the script file `addAmountAndUnitFromName` into it
2. Put the input CSV file in there too, let's say its name is `out.csv`
3. Open CMD in that folder and run the command below
4. Syntax: `node addAmountAndUnitFromName out.csv 2`

Parameters:
- `node` = the program that runs the script
- `addAmountAndUnitFromName` = the name of the script file
- `out.csv` = the name of the input file
- `1` = the index number of the name column in your input file

## Example:
- If the content of the out.csv file is:
```csv
12345;Cocacola 500ml;50.00;Nápoje
67890;Pepsi 500ml;50.00;Nápoje
```

- Then the command should be:
```bash
node addAmountAndUnitFromName out.csv 1
```

- because the index number of the name column is 1 (counted from 0)

This command will produce a new file called out.csv in the same folder, which will contain the amount and unit columns.

## Syntax: 
```bash
node addAmountAndUnitFromName out.csv 1 "\t"
```

or

```bash
node addAmountAndUnitFromName out.csv 1 ";"
```

or just

```bash
node addAmountAndUnitFromName out.csv 1 
```

if you don't specify the delimiter, it will use "\t" 