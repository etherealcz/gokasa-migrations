# Migration from iProsoft

### New iprosoft 2024 migrations
- Go to products, find products and export via the Excel button - you will get a `.csv` file
- Use Excel to import data from this `.csv` file and work from there
- Find and remove all semi-colons (;) and quotes (") from the excel sheet
- Remove all rows with EANs which includes a non-digit symbol
- Remove all rows with EANs which are short (1-2 symbols)
- Remove all rows with the stock value of 0.000
- Rearrange and rename column to: ean, name, price, group, amount, unit 
- Deduplicate group values to get a list of unique group names then use VLOOKUP to create a new column with the correct GOKASA group number values
- Columns amount and unit are empty - to fill them automatically, use the addAmountAndUnit tool in partners register settings, or run `parse2.bat`



## DEPRECATED BELOW

### Before you start
- Install NodeJS from https://nodejs.org to your computer
- Install MS Excel, or you can use Google Sheets instead
- Download this repository and extract the archive to a folder on your desktop, i.e. `C:\Users\User\Desktop\gokasa-migrations`

### Steps:
- Connect to the client's PC and install SQLite DB Browser there
    - download it from [https://sqlitebrowser.org/](https://sqlitebrowser.org/)
    - or for Win64 [https://drive.google.com/file/d/1EkfDlZKqDgc7VT7hVw4GbFrH783Et7uE/view?usp=sharing](https://drive.google.com/file/d/1EkfDlZKqDgc7VT7hVw4GbFrH783Et7uE/view?usp=sharing)
    - or for Win32 [https://drive.google.com/file/d/1raHvo21SZMR7hh6Pr4vGtyJg73ozWIAs/view?usp=sharing](https://drive.google.com/file/d/1raHvo21SZMR7hh6Pr4vGtyJg73ozWIAs/view?usp=sharing)
- Create a backup file in iProsoft (Menu/Tools/Backup) this will create a `.db3` file
- Open this file in SQLite DB Browser
- Run the following query (in `Execute SQL tab`)
```sql
CREATE TABLE export AS
SELECT 
  Barcode.Value, 
  Product.Name, 
  Product.SalePrice, 
  ProductCategory.Name, 
  Product.PriceLabelAmount, 
  Product.PriceLabelUnit 
FROM 
 Product, Barcode, ProductCategory 
WHERE 
 Product.SalePrice != 0
 AND Product.Id = Barcode.ProductId 
 AND Product.ProductCategoryId = ProductCategory.Id
```
- Select `File / Export / Table to JSON`, select table `export`, save as JSON file `export.json` and copy this file over to your PC to the folder `C:\Users\User\Desktop\gokasa-migrations\iprosoft`.
- In the `C:\Users\User\Desktop\gokasa-migrations\iprosoft` folder, run the `parse1.bat` file.
    - this will give you file `out.csv`
- Open Excel, convert column data types of the first 6 columns to `text` so you don't loose the format
- Copy and paste the content of file `out.csv` (from notepad) to that Excel list
- Things to in Excel: 
    - remove duplicates (based on EAN value) - there cannot be two or more rows with the same EAN value, 
    - remove unwanted characters in the `name` column, 
    - replace decimal comma with decimal point in the `price` column, 
    - convert group names to numbers according to group numbers on the user's Gokasa account
    - check ean codes validity, format, etc.
- Once you are happy with the content, Ctrl+A and copy and paste it over to a new text file in the same folder and name it `input.csv` and save it there
- Run the `parse2.bat` file in terminal
    - this will give you file `out.csv` including the last two columns (amount and unit)
- Copy and paste the `out.csv` file content back to Excel and correct the column headers
- Copy and paste the Excel content to file `out.csv`, then this file can be used to import to Gokasa

### Stock balances and stock-taking
- Follow the instructions here https://docs.google.com/spreadsheets/d/1PvoX7mdHtS9vR1HP16DwxXO2gBRZhTbhxo-rstD7FXI (in the hidden sheets)
