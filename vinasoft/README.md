# Export and parse from Vinasoft

- You'll need nodejs installed in your computer, executable via PATH, e.g. run `node --version` to check
- Vinasoft uses password (51224999) protected MySQL, therefore we need to allow access to MySQL without password on Windows:
    1. shut down service mysql57 in services.msc
    1. go to `C:\ProgramData\MySQL\MySQL Server 5.7` (ProgramData is a hidden folder)
    1. look for file `my.ini`, open it in notepad++ and add one line `skip-grant-tables` below `[mysqld]`, and save, then restart service mysql57 in services.msc
        1. IMPORTANT: if you are on Windows 7, edit it in [notepad++](https://notepad-plus-plus.org/downloads/) in UTF-8 mode! because regular Windows notepad will destroy this file 

```ini
...
[mysqld]

skip-grant-tables
...
```

- Open a MySQL client like MySQL Workbench, and connect to MySQL using root account
- open query tab
- clear and empty the tab first
- set no limit
- execute the following command (on table `pos.zbozi` or `pos.hang`)
```sql
SELECT kod, nazev, cena, kategorieDPH, vaha, druhHmotnost FROM pos.zbozi WHERE cena > 0;
```

- export the results to a json file named `data.json` and place it in this folder
- open `data.json` in text editor and fix JSON format (replace `NULL` with `""` and `'` with `"`)
- run `node prepare-groups.js`
- open file `groups.json` in text editor and change the `true` values to Gokasa group numbers
```json
{
  "Potraviny": 1,
  "Drogerie": 11,
  "Alkohol": 6,
  "Ovoce": 10,
  "Tabak": 7,
  "Cigarety": 7,
  "Pecivo": 1,
  "...": "..."
}
```
- if needed, you can also run `node prepare-units.js` and then map the unit values in file `units.json`
```json
{
  "": "pcs",
  "ks": "pcs",
  "ml": "ml",
  "gram": "g",
  "par": "pcs",
  "litr": "l",
  "bal": "pcs",
  "kg": "kg",
  "metr": "m",
  "cm": "cm"
}
```
- then run `node parse.js` to create the file for import
- use file `catalog.csv` to import to Gokasa

# How to restore backup for Vinasoft
- exit Vinasoft
- choose a backup file in `C:\vinasoft\DATA\yyyy-mm-dd\file.zip`
- extract the zip file using WinRAR, password is `THANGVINASOFT2017`
- combine all files here to a single `dump.sql` file using cmd `copy /b *.txt dump.sql`
- in MySQL Workbench, select all tables in database `pos`, right-click, select `Drop (n) tables`
- use the `dump.sql` file to import in MySQL Workbench
    - Menu > Server > Data import > Import from Self-cointained file > Select file `dump.sql` > click Start import
- run thr following SQL query
```sql
UPDATE `pos.funkce` SET `DBZkontrolovan`='0' WHERE `ID`='1'
```
- open Vinasoft