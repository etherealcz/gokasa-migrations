
Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d === undefined ? '.' : d,
        t = t === undefined ? '' : t,
        s = n < 0 ? '-' : '',
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + '',
        j = (j = i.length) > 3 ? j % 3 : 0;
    var retval = s + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '');
    if (retval === '-0.00') {
        retval = '0.00';
    }
    return retval;
};

var fs = require('fs');
var data = JSON.parse(fs.readFileSync('data.json'));
var groups = JSON.parse(fs.readFileSync('groups.json'));
var units = JSON.parse(fs.readFileSync('units.json'));
var articles = [];
data.filter(function (item) {
    return /^\d{3,20}$/.test(item.kod);
}).forEach(function (item) {
    var article = {
        ean: item.kod,
        name: item.nazev.trim().replace(/\s+/g, ' ').replace(/;/g, ''),
        price: item.cena.formatMoney(),
        group: groups[item.kategorieDPH],
        amount: item.vaha || 1,
        unit: units[item.druhHmotnost] || 'pcs',
    }
    articles.push(article);
});
var headers = ['ean', 'name', 'price', 'group', 'amount', 'unit'];
var out = articles.map(function (article) {
    return headers.map(function (header) {
        return article[header];
    }).join('\t');
}).join('\n');

fs.writeFileSync('catalog.csv', headers.join('\t') + '\n' + out);
